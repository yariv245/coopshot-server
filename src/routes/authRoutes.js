const express = require("express");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = mongoose.model("User");

const router = express.Router();

router.post("/signup", async (req, res) => {
  const { email, password, phone, name } = req.body;
  try {
    const user = new User({ email, password, phone, name });
    await user.save(); // if the email already exist it will throw exception
    const idReturn = user._id;
    const defaultImageUrl =
      "https://res.cloudinary.com/yarivmenachem/image/upload/v1621423371/profle_zv2uxy.png";
    const token = jwt.sign(
      { userId: user._id, image: defaultImageUrl },
      "MY_SECRET_KEY"
    );
    res.send({ token, userID: idReturn });
  } catch (err) {
    return res.status(422).send(err.message);
  }
});

router.post("/signin", async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).send({ error: "Must provide email and password" });
  }

  const user = await User.findOne({ email });

  if (!user) {
    return res.status(422).send({ error: "Invalid email" });
  }

  try {
    await user.comparePassword(password);
    const token = jwt.sign({ userId: user._id }, "MY_SECRET_KEY");
    const userID = user.id;
    res.send({
      token,
      userID,
      name: user.name,
      phone: user.phone,
      image: user.imageUrl,
      favIds: user.favorite,
    });
  } catch (err) {
    return res.status(422).send({ error: "Invalid password or email" });
  }
});

module.exports = router;
